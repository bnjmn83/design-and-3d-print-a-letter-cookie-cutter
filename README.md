# Design and 3D print a cookie cutter from any text

How to design and 3D print a cookie cutter from a given text.

## Tools Used
* Inkscape
* Fusion 360
* Slic3r
* Tinkercad (optional)

## Create Text
* Open Inkscape
* Create text, apply font and set a big size (e.g. 200)
* Duplicate Text (_ctrl+d_)
* Pick different color from bottom for new shape
* Edit preferences: 
 * _Edit->Prefernces->Behavior->Steps->Inset/Outset by_: 0,1 mm
* Select upper shape
* Apply Path->Inset (_ctrl+(_) to to create outlines
 * Repeat if outlines too small. This process will define the thickness of your cookie cutter perimeter.
* Adjust _Steps->Inset/Outset_ if outlines look too messy or need to be more granular
* Select all objects (_ctrl+a_) and select _Object->Group_
* Select object and apply _Path->Object to Path_
* Save as SVG file

## Post Processing in Fusion 360

## Post Processing in Tinkercad
* Open SVG in https://www.tinkercad.com/
* Export to STL to view in slicing tool